package it.anoki.model;

import java.util.ArrayList;
import java.util.List;

public class Azienda {
	private List<Dipendente> dipendentiAzienda;
	
	public Azienda(List<Dipendente> dipendentiAzienda) {
		this.dipendentiAzienda = dipendentiAzienda;
	}

	public void calcolaStipendi(){
		for(Dipendente dipendente : this.dipendentiAzienda){
			dipendente.print();
		}
		
	}
	
	public List<Dipendente> getDipendentiByPosizione(Posizione posizione){
		List<Dipendente> dipendenti= new ArrayList<Dipendente>();
		for(Dipendente dipendente : this.dipendentiAzienda){
			if(dipendente.getPosizione()==posizione){
				dipendenti.add(dipendente);
			}
		}
		return dipendenti;
		
	}
	
	
	
}
