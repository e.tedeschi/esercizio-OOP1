package it.anoki.model;

public class Venditore extends Dipendente {
	//Stipendio base del venditore
	private final float stipendioBase = 500.00f;
	//Percentuale sulle vendite che spetta ail venditore
	private final float percentualeVendite = 0.005f;
	
	private float venditeMensili;

	public Venditore(String nome, float venditeMensili) {
		this.posizione=Posizione.VENDITORE;
		this.setStipendioBase(stipendioBase);
		this.venditeMensili = venditeMensili;
		this.nome = nome;
	}

	public float getVenditeMensili() {
		return venditeMensili;
	}

	public void setVenditeMensili(float venditeMensili) {
		this.venditeMensili = venditeMensili;
	}

	public float getStipendiobase() {
		return stipendioBase;
	}

	public float getPercentualevendite() {
		return percentualeVendite;
	}
	/**
	 * Lo stipendo del venditore � dato dalla somma del suo stipendio base
	 * e una percentuale dello 0,5% sulle vendite effettuate
	 */
	@Override
	public float calcolaStipendio() {
		float incrementoBaseVendite = this.venditeMensili * percentualeVendite;
		return stipendioBase + incrementoBaseVendite;
	}

}
