package it.anoki.model;

import java.util.List;

public class Manager extends Dipendente{

	private final float stipendioBase = 1500.00f;
	private List<Dipendente> sottoposti;
	private static final float percentualeSuiSottoposti=0.1f;

	
	public Manager(String nome, List<Dipendente> sottoposti) {
		this.setStipendioBase(stipendioBase);
		this.posizione=Posizione.MANAGER;
		this.nome=nome;
		this.sottoposti=sottoposti;
	}
	/**
	 * Lo stipendio del manager � dato dalla somma del suo stipendio base
	 * e una percentuale del 10% sullo stipendio base dei suoi sottoposti
	 */
	@Override
	public float calcolaStipendio() {
		float stipendioTotale=stipendioBase;
		float stipendioSottoposto=0f;
		for(Dipendente dipendente: this.sottoposti){
			stipendioSottoposto = dipendente.getStipendioBase();
			stipendioTotale+=stipendioSottoposto*percentualeSuiSottoposti;
		}
		return stipendioTotale;
	}
	
	public List<Dipendente> getSottoposti() {
		return sottoposti;
	}



	public void setSottoposti(List<Dipendente> sottoposti) {
		this.sottoposti = sottoposti;
	}

	public  float getStipendiobase() {
		return stipendioBase;
	}




}
