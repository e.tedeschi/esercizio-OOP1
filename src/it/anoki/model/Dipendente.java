package it.anoki.model;

public abstract class Dipendente {

	private float stipendioBase;
	public String nome;
	public abstract float calcolaStipendio();
	protected Posizione posizione;
	
	/**
	 * metodo per il calco degli stipendi
	 * @return
	 */
	public float getStipendioBase() {
		return stipendioBase;
	}

	public void print(){
		System.out.println("Posizione: "+this.getPosizione());
		System.out.println("Nome: "+this.getNome());
		System.out.println("Stipendio: "+this.calcolaStipendio());
		System.out.println("----------------------------------------------------");
	
	}
	public void setStipendioBase(float stipendioBase) {
		this.stipendioBase = stipendioBase;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Posizione getPosizione() {
		return posizione;
	}

	public void setPosizione(Posizione posizione) {
		this.posizione = posizione;
	}
	
	
}
