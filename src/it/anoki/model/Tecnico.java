package it.anoki.model;

public class Tecnico extends Dipendente {

	private static final float stipendioBase = 1400.00f;
	
	public float venditeMensili;

	public Tecnico(String nome) {
		this.posizione= Posizione.TECNICO;
		this.setStipendioBase(stipendioBase);
		this.nome=nome;
		
	}

	@Override
	public float calcolaStipendio() {
		return this.getStipendioBase();
	}

	public float getVenditeMensili() {
		return venditeMensili;
	}

	public void setVenditeMensili(float venditeMensili) {
		this.venditeMensili = venditeMensili;
	}
}
