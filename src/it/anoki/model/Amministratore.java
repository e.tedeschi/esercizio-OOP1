package it.anoki.model;

import java.util.List;

public class Amministratore extends Manager {

	public Amministratore(String nome, List<Dipendente> sottoposti) {
		super(nome, sottoposti);
		this.posizione=Posizione.AMMIISTRATORE;
	}

}
