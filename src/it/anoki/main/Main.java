package it.anoki.main;

import java.util.ArrayList;
import java.util.List;

import it.anoki.model.Amministratore;
import it.anoki.model.Azienda;
import it.anoki.model.Dipendente;
import it.anoki.model.Manager;
import it.anoki.model.Tecnico;
import it.anoki.model.Venditore;

public class Main {

	public static void main(String[] args) {
		List<Dipendente> dipendenti = new ArrayList<Dipendente>();
		List<Dipendente> sottopostiMario = new ArrayList<Dipendente>();
		List<Dipendente> sottopostiMaria = new ArrayList<Dipendente>();
		Dipendente vittorio= new Venditore("Vittorio",15000f);
		Dipendente teresa= new Tecnico("Teresa");
		sottopostiMario.add(vittorio);
		sottopostiMario.add(teresa);
		Dipendente mario= new Manager("Mario",sottopostiMario);
		dipendenti.addAll(sottopostiMario);
		Dipendente virna = new Venditore("Virna",17000f);
		sottopostiMaria.add(virna);
		sottopostiMaria.add(mario);
		Dipendente maria = new Amministratore("Maria", sottopostiMaria);
		dipendenti.addAll(sottopostiMaria);
		dipendenti.add(maria);
		Azienda azienda = new Azienda(dipendenti);	
		azienda.calcolaStipendi();
	}

}
