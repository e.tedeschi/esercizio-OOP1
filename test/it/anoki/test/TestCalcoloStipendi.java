package it.anoki.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.anoki.model.Amministratore;
import it.anoki.model.Dipendente;
import it.anoki.model.Manager;
import it.anoki.model.Tecnico;
import it.anoki.model.Venditore;

public class TestCalcoloStipendi {

	private Dipendente managerSenzaSottoposti;
	private Dipendente managerConSottoposti;
	private Dipendente tecnico;
	private Dipendente venditore;
	private Dipendente amministratore;

	private static final float TC_01_STIPENDIO_TECNICO = 1400f;
	private static final float TC_02_STIPENDIO_VENDITORE = 550f;
	private static final float TC_04_STIPENDIO_MANAGER = 1690f;
	private static final float TC_05_STIPENDIO_AMMINISTRATORE=1700f; 

	@Before
	public void init() {
		String nomeTecnico = "nomeTecnico";
		tecnico = new Tecnico(nomeTecnico);
		String nomeVenditore = "nomeVenditore";
		float venditeMensili = 10000f;
		venditore = new Venditore(nomeVenditore, venditeMensili);
		List<Dipendente> sottoposti = new ArrayList<Dipendente>();
		String nomeManagerSenzaSottoposti = "nomeManagerSenzaSottoposti";
		managerSenzaSottoposti = new Manager(nomeManagerSenzaSottoposti, new ArrayList<Dipendente>());
		String nomeManagerConSottoposti = "nomeManagerConSottoposti";
		sottoposti.add(tecnico);
		sottoposti.add(venditore);
		managerConSottoposti = new Manager(nomeManagerConSottoposti, sottoposti);
		String nomeAmministratore = "nomeAmministratore";
		List<Dipendente> sottopostiAmministratore = new ArrayList<Dipendente>();
		sottopostiAmministratore.add(managerConSottoposti);
		sottopostiAmministratore.add(venditore);
		amministratore = new Amministratore(nomeAmministratore, sottopostiAmministratore);
	}

	@Test
	public void TC_01_calcolo_stipendio_tecnico() {
		assertEquals(tecnico.calcolaStipendio(), TC_01_STIPENDIO_TECNICO, 0);
	}

	@Test
	public void TC_02_calcolo_stipendio_venditore() {
		assertEquals(venditore.calcolaStipendio(), TC_02_STIPENDIO_VENDITORE, 0);
	}

	/**
	 * Verifichiamo che per un manager senza sottoposti venga calcolato
	 * correttamente lo stipendio
	 */
	@Test
	public void TC_03_calcolo_stipendio_manager_senza_sottoposti() {
		assertEquals(managerSenzaSottoposti.calcolaStipendio(), managerSenzaSottoposti.getStipendioBase(), 0);
	}

	@Test
	public void TC_04_calcolo_stipendio_manager_con_sottoposti() {
		assertEquals(managerConSottoposti.calcolaStipendio(), TC_04_STIPENDIO_MANAGER, 0);
	}
	
	@Test
	public void TC_05_calcolo_stipendio_amministratore(){
		assertEquals(amministratore.calcolaStipendio(),TC_05_STIPENDIO_AMMINISTRATORE, 0 );
	}
}
