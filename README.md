Il seguente applicativo consente il calcolo degli stipendi per un insieme di dipendenti della stessa azienda.

I dipendenti possono essere classificati nelle seguenti categorie:
* Manager
* Venditore
* Tecnico.

Gli stipendi sono calcolati con la seguente logica:
* Tecnico -> ha solo uno stipendio base
* Venditore -> ha uno stipendio base incrementato dello 0,5% del totale delle sue vendite
* Manager -> ha uno stipendio base incrementato del 10% dello stipendio base di ciascun suo sottoposto.
* Amministratore ->  ha uno stipendio base incrementato del 10% dello stipendio base di ciascun suo sottoposto.

L'analisi effettuata prevede l'individuazione delle seguenti entit�:
* Tecnico
* Venditore
* Manager
* Amministratore

Le entit� individuate presentano un insieme di attributi comuni quali:
* Nome
* Posizione
* Stipenio base

Inoltre, per ciascuna entit� � prevista una diversa formula per il calcolo dello stipendio sulla base delle logiche descritte.

L'idea implementativa consiste nel definire la classe astratta Dipendente che raccoglie tutti gli attributi comumi a ciascuna entit� e la definizione di un metodo astratto per il calcolo degli stipendi.

Le varie entit� vengono implementate estendendo la classe astratta Dipendente e implementandone il metodo per il calcolo degli stipendi.


Per completezza implementativa sono stati definiti dei test di unit� per i vari metodi di calcolo degli stipendi.



 
